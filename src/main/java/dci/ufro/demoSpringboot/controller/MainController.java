package dci.ufro.demoSpringboot.controller;

import dci.ufro.demoSpringboot.model.Persona;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

@Controller
public class MainController {

    ArrayList<Persona> personas=new ArrayList<>();

    @RequestMapping(value="" , method = RequestMethod.GET)
    public String addPersonaGet(Model model ){
        String titulo="Add persona";

        model.addAttribute("titulo",titulo);
        return "/add";
    }

    @RequestMapping(value="" , method = RequestMethod.POST)
    public String addPersonaPost(Model model,
                                   @RequestParam String nombres,
                                   @RequestParam String apellidos,
                                   @RequestParam String rut,
                                   @RequestParam String telefono){
        Persona persona=new Persona();

        if(nombres!=null && apellidos!=null && rut!=null && telefono!=null){
            persona.setNombres(nombres);
            persona.setApellidos(apellidos);
            persona.setRut(rut);
            persona.setTelefono(telefono);
            this.personas.add(persona);
            model.addAttribute("personas",this.personas);
            return "/index";
        }

        return "/";
    }

    @GetMapping(value="persona")
    public String mostrarPersona(Model model ){
        String titulo="Persona";
        Persona persona = new Persona();
        model.addAttribute("persona",persona);
        model.addAttribute("titulo",titulo);
        return "/index";
    }

}
